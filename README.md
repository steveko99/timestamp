# timestamp

To use:
- copy timer.h and #include
- copy timer.c and compile
- see the makefile for building 

Test cases:
- The makefile builds the tests and compiles timer.c
- -DDEBUG and -DTIMERS should enable the timers
- without DEBUG and TIMERS set, the timer apis should not be present in the executable

To test that the timer calls are or are not in the executable, run:
- $ readelf -s test1 | grep timer

Tested on Ubuntu 18

- Macros 
https://stackoverflow.com/questions/1644868/define-macro-for-debug-printing-in-c
