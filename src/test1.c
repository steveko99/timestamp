#include <stdio.h>
#include "timer.h"

#define GIGBYTE 1073741824

static void loop_gigs(int nGig)
{
    for ( int i=0; i<nGig; i++ )
        for ( int j=0; j<GIGBYTE; j++ )
            ;
}

static void Test_900()
{
    printf("Test_900\n");
    for ( int i=0; i<128; i++ ) {
        printf("Try %d gig loops\n", i);
        TIMER_START(900);
        loop_gigs(i);
        TIMER_END(900);
    }
}

static void Test_01()
{
    printf("Test_01\n");
    // simple test: use 4 slots
    TIMER_START(301);
    TIMER_START(302);
    TIMER_START(303);
    TIMER_START(304);
    TIMER_END(301);
    TIMER_END(302);
    TIMER_END(303);
    TIMER_END(304);
}

static void Test_02()
{
    printf("Test_02\n");
    /* negative tests: have to uncomment 1 at a time and rebuild to test these */

    /* repeat start: finds the same slot and updates the timestamp */
    // TIMER_START(301);
    // TIMER_START(301);

    /* timer_start cannot allow id == 0 */
    // TIMER_START(0);

    /* timer_end cannot allow id == 0 */
    // TIMER_END(0);

    /* end that does not exist: raises assertion */
    // TIMER_END(302);

    /* repeat end: assertion failure is expected */
    // TIMER_END(301);
    // TIMER_END(301);

    /* too many open timers at once: raises assertion */
    // for ( int i=100; i<200; i++ ) { TIMER_START(i); }
}

int main(int argc, char *argv[])
{
    printf("main\n");
    Test_01();
    Test_02();
    Test_900();
}

