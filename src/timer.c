/*
 * timer.c
 *
 * In-Memory timestamp library
 *
 * timer_start(id):
 *     id - must be unique for all timers running simultaneously
 *          id cannot be zero
 *          if id is already in use, fails assertion and exits
 *
 * long int timer_end(id):
 *     id - must be an open timer
 *          fails assertion and exits if timer is not found
 *
 * There is a max number of timers that can be started simultaneously, see NUM_TIMERS.
 *
 */

#include "timer.h"


/* not standard START */
#define _GNU_SOURCE
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>

int gettid() {
    pid_t tid;
    tid = syscall(SYS_gettid);
    return (tid);
}
/* not standard END */



/* Disable all the timer calls if building with TIMERS off */

#ifdef TIMERS

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <pthread.h> 

typedef struct timespec TSTAMP;

typedef struct timer {
    int id;             /* value chosen by client, used as a tag */
    int thread_id;      /* Linux thread_id to support multi-threaded */
    TSTAMP timestamp;   /* the saved timestamp for this id */
} TIMER;

static TIMER g_timers[NUM_TIMERS] = { 0 };          /* All slots zeroed */

/*
 * serialize search/alloc/free changes to g_timers[]
 */
static pthread_mutex_t g_TableLock = PTHREAD_MUTEX_INITIALIZER;

/*
 * struct timespec to I64 (large int) in nano-sec
 */
#define TS_TO_I64(ts)    ( 1000000000 * (long int) ts.tv_sec + (long int) ts.tv_nsec )

static TSTAMP tnow()
{
    TSTAMP ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    return ts;
}

/*
 * If id == 0, then search for an empty slot
 * If id != 0, then search for the slot where id and threadid match
 * Returns -1 if not found
 * Returns index of slot if found ( rval >= 0 )
 */
static int find_slot(int id)
{
    int slot = -1;
    int tid = gettid();

    if ( id == 0 ) {
        tid = 0;
    }

    for ( int i=0; i<NUM_TIMERS; i++ ) {
        if ( g_timers[i].id == id && g_timers[i].thread_id == tid ) {
            slot = i;
            break;
        }
    }
    return slot;
}

/*
 * Allocate a new slot for 'id' and save tnow
 */
void timer_start(int id)
{
    assert(id);

    /* check for a double start */
    int slot = find_slot(id);
    assert(slot < 0);

    /* finding and freeing or allocating a slot must be atomic */
    pthread_mutex_lock(&g_TableLock);

    slot = find_slot(0);
    assert(slot >= 0);

    int tid = gettid();

    /* allocate the slot */
    g_timers[slot].id = id;
    g_timers[slot].thread_id = tid;

    pthread_mutex_unlock(&g_TableLock);

    /* sample and save t0 */
    TSTAMP ts = tnow();
    memcpy(&g_timers[slot].timestamp, &ts, sizeof(TSTAMP));

    TRACE("timer_start: id=%d tid=%d slot=%d\n", id, tid, slot);
}

/*
 * Finds the slot for 'id' and returns (tnow - t0)
 * Return value is the number of nano-seconds since t0 (INT64)
 */
long int timer_end(int id)
{
    TSTAMP t1 = tnow();
    assert(id);

    /* finding and freeing or allocating a slot must be atomic */
    pthread_mutex_lock(&g_TableLock);

    int slot = find_slot(id);
    assert(slot >= 0);

    TSTAMP t0 = g_timers[slot].timestamp;

    g_timers[slot].timestamp.tv_sec = 0;
    g_timers[slot].timestamp.tv_nsec = 0;

    /* free the slot */
    g_timers[slot].thread_id = 0;
    g_timers[slot].id = 0;

    pthread_mutex_unlock(&g_TableLock);

    /* compute and return difference */
    long int dt = TS_TO_I64( t1 ) - TS_TO_I64( t0 );
    TRACE("timer_end: id=%d slot=%d dt=%ld\n", id, slot, dt);
    return dt;
}

#endif
