/*
 * timer.h
 *
 * Exported API For client code 
 */

/*
 * id can be any value except 0
 */
void timer_start(int id);

/*
 * id can be any value except 0
 * returns (tnow - t0) duration in nano-seconds, type is I64 (long int)
 */
long int timer_end(int id);

/*
 * get thread ID
 */
int gettid();

/*
 * macro to convert long int nano-seconds to a double in seconds
*/
#define NSEC_TO_DOUBLE(x) ( (double) x / (double) 1000000000.0 )

/*
 * Max number of simultaneous timers
 */
#define NUM_TIMERS     8

/*
 * TRACE(fmt, ...)
 */
#ifdef DEBUG
#define DEBUG_PRINT 1
#else
#define DEBUG_PRINT 0
#endif

#define TRACE(fmt, ...) do { if (DEBUG_PRINT) fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__, __LINE__, __func__, __VA_ARGS__); } while (0)

/*
 * TIMER_START(id)
 * TIMER_END(id)
 */
#ifdef TIMERS
#define TIMERS_ON 1
#else
#define TIMERS_ON 0
#endif

#define TIMER_START(id) do { if (TIMERS_ON) timer_start(id); } while(0)
#define TIMER_END(id) do { if (TIMERS_ON) TRACE("duration = %lf (sec)\n", NSEC_TO_DOUBLE(timer_end(id))); } while(0)

