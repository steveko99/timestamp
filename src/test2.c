#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include "timer.h"

static pthread_t g_tid[NUM_TIMERS];

#define OVERALL_TIMER -1

void* thread_entry(void *arg)
{
    int tid = gettid();

    TRACE("Job %d started\n", tid);

    TIMER_START(tid);

    for(int i=0; i<(1024 * 1024 * 1024); i++);    // do work

    TIMER_END(tid);

    return NULL;
}

int main(void)
{
    int err;

    /* Start an overall timer */
    TIMER_START(OVERALL_TIMER);

    /* Reduce by 1 because of the overall timer */
    int nt = NUM_TIMERS - 1;

    printf("spawning %d threads\n", nt);

    for(int i=0; i < nt; i++ ) {
        err = pthread_create(&(g_tid[i]), NULL, &thread_entry, NULL);
        if (err != 0) {
            fprintf(stderr, "ERROR: create thread : %s\n", strerror(err));
            return 1;
        }
    }

    printf("wait for %d threads to complete\n", nt);
    for(int i=0; i < nt; i++ ) {
        pthread_join(g_tid[i], NULL);
    }

    TIMER_END(OVERALL_TIMER);

    printf("Done\n");
    return 0;
}
